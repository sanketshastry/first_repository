package com.jpm.selenium.popup;

import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.jpm.selenium.util.ChromeUtil;

public class PopUpWindowDemo {

	public static void main(String[] args) throws InterruptedException {
		// Load driver and open browser
		WebDriver driver =ChromeUtil.getChromeDriver();
		String url="file:\\D:\\Sanket\\Day 43 selenium\\11_SeleniumWebdriver\\src\\main\\java\\PopUpWinDemo.html";
		driver.get(url);
		String parentWin=driver.getWindowHandle();
		System.out.println("Parent Win:"+parentWin);
		Thread.sleep(2000);
		
		driver.findElement(By.id("newtab")).click();
		parentWin=driver.getWindowHandle();//parent window
		System.out.println("Parent Win:"+parentWin);
		Thread.sleep(2000);
		
		Set<String> childWinList=driver.getWindowHandles();//All the windows used
		for(String childWin : childWinList){
			if(!childWin.equals(parentWin)){
				Thread.sleep(2000);
				driver.switchTo().window(childWin);
				System.out.println("Child Win:"+childWin);
				driver.findElement(By.id("alert")).click();
				Thread.sleep(2000);
				Alert alert =driver.switchTo().alert();
				Thread.sleep(2000);
				alert.accept();//use dismiss() to cancel
			}
		}
		
		
		Thread.sleep(2000);
		
		driver.switchTo().window(parentWin);
		System.out.println("Switch to Parent Win:"+ parentWin);
		Thread.sleep(2000);
		System.out.println("Again click to another child win");
		driver.findElement(By.id("newwindow")).click();
		
		for(String childWin2 : driver.getWindowHandles()){
			if(!childWin2.equals(parentWin)){
				Thread.sleep(2000);
				driver.switchTo().window(childWin2);
				System.out.println("Child Win:"+childWin2);
				Thread.sleep(2000);
				
			}
				
				
		
		
		
		
	}
		Thread.sleep(2000);
		driver.quit();
	}
}
