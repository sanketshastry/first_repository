
package com.jpm.selenium.driver_cmd;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.jpm.selenium.util.ChromeUtil;

public class WebDriverCommands {

	public static void main(String[] args) throws InterruptedException {
		// we need instance of driver
		WebDriver driver = ChromeUtil.getChromeDriver();
		if(driver!=null){
			//get->launch the website
			//String url="https://synergetics-india.com/";
			String url="file:\\D:\\Sanket\\Day 43 selenium\\11_SeleniumWebdriver\\src\\main\\java\\Locators.html";
			driver.get(url);
			
			//lets get the title of the web page
			System.out.println("Title of the page is:"+driver.getTitle());
			System.out.println("Current URL:"+driver.getCurrentUrl());
			
			//By.id->Elements by id
			WebElement element=driver.findElement(By.id("user"));
			System.out.println("element 1:"+element.getAttribute("id"));
			element.sendKeys("admin");
			
			//input password by name
			driver.findElement(By.name("password")).sendKeys("password@123");
			
			//By link text
			element=driver.findElement(By.linkText("How to use locators?"));
			System.out.println("element link text:"+element.getText());
			Thread.sleep(2000);
			//it will click the link and the web element is holding link reference
			element.click();
		}
		else{
			System.out.println("Driver not loaded!");
		}
		Thread.sleep(5000);
		driver.close();
	}

}
