package com.jpm.selenium.locators;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.jpm.selenium.util.ChromeUtil;

public class TestLocators {

	public static void main(String[] args) throws InterruptedException {
		// Load the driver
		WebDriver driver =ChromeUtil.getChromeDriver();
		//open the page
		String url="http://demo.opencart.com";
		driver.get(url);
		Thread.sleep(2000);
		
		WebElement searchBox=driver.findElement(By.name("search"));
		searchBox.sendKeys("phone");
		Thread.sleep(2000);
		
		driver.findElement(By.className("input-group-btn")).click();
		
		//input-search
		WebElement searchBox1 = driver.findElement(By.id("input-search"));
		searchBox1.clear();
		searchBox1.sendKeys("mac");
		Thread.sleep(2000);
		//button-search
		////*[@id="button-search"]
		driver.findElement(By.xpath("//*[@id=\'button-search\']")).click();
		Thread.sleep(2000);
		
		//navigate back,forward,to,refresh
		driver.navigate().back();
		Thread.sleep(2000);
		
		//By Tag Name
		List<WebElement> listAnchor= driver.findElements(By.tagName("a"));
		System.out.println("List of 'a' tag");
		/*for(WebElement temp:listAnchor){
			System.out.println(temp.getText());
		}
		*/
		//java 8 lambda
		listAnchor.forEach(temp->System.out.println(temp.getText()));
		
		
		//By Css Selector
		List<WebElement> listCss = driver.findElements(By.cssSelector("span.price-tax"));
		System.out.println("List of CSS Selectors");
		listCss.forEach(action->System.out.println(action.getText()));
		
		Thread.sleep(10000);
		driver.close();

	}

}
