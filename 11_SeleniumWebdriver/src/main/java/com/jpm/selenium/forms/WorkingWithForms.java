package com.jpm.selenium.forms;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.jpm.selenium.util.ChromeUtil;

public class WorkingWithForms {

	public static void main(String[] args) throws InterruptedException {
		//load driver and open browser
		WebDriver driver = ChromeUtil.getChromeDriver();
		//open the webpagae 
		driver.get("D:\\Sanket\\Day 43 selenium\\11_SeleniumWebdriver\\src\\main\\java\\WorkingWithForms.html");
		//maximize the browser window
		driver.manage().window().maximize();
		
		driver.findElement(By.id("txtUserName")).sendKeys("JPM_Sanket");
		driver.findElement(By.name("txtPwd")).sendKeys("jpm@123");//pwd
		driver.findElement(By.className("Format")).sendKeys("jpm@123");//confirm pswd
		driver.findElement(By.cssSelector("Input.Format1")).sendKeys("Sanket");
		driver.findElement(By.cssSelector("Input#txtLastName")).sendKeys("Shastry");
		
		
		List<WebElement> radioElements=driver.findElements(By.name("gender"));
		for(WebElement radio:radioElements){
			String radioSelection=radio.getAttribute("value").toString();
			//which radio button to be selected.
			if(radioSelection.equals("Male")){
				radio.click();
			}
		}
		driver.findElement(By.cssSelector("input[type=date]")).sendKeys("12/21/2011");
		
		driver.findElement(By.id("txtEmail")).sendKeys("sanketshastry@gmail.com");
		//dropdown
		driver.findElement(By.name("Address")).sendKeys("Hyderabad");;
		Select drpCity= new Select(driver.findElement(By.name("City")));
		//Dropdown 3 ways-> selectByValue,selectByIndex and selectByVisibleText 
		drpCity.selectByIndex(3);
		drpCity.selectByValue("Mumbai");
		drpCity.selectByVisibleText("Pune");
		
		
		driver.findElement(By.name("Phone")).sendKeys("9985781802");;
		
		
		List<WebElement> checkEleList= driver.findElements(By.name("chkHobbies"));
		for(WebElement hobbyChkBox:checkEleList){
			
			String selection = hobbyChkBox.getAttribute("value").toString();
			if(!selection.equals("Movies")){//not click Movies
				hobbyChkBox.click();
			}
		}
		
		
		Thread.sleep(10000);
		driver.close();
		System.out.println("Registration on process");
		
	}

}
